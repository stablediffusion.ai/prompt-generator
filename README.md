# Prompt Generator



## Getting started

This is a demo of the model series: “MagicPrompt”, in this case, aimed at: Stable Diffusion. To use it, simply submit your text or click on one of the examples.

To learn more about the model, go to the link: https://huggingface.co/Gustavosta/MagicPrompt-Stable-Diffusion

Demo available at : https://stablediffusion.fr/prompt-generator
